Pod::Spec.new do |s|

  ### Summary information
  s.name         = "CMTFilterEngineIOSStatic"
  s.version      = "0.80.0"
  s.summary      = "CMT real-time sensor processing library framework"
  s.description  = <<-DESC
                   Cambridge Mobile Telematics Real-time sensor processing library 
                   DESC
  s.homepage     = "http://www.cmtelematics.com"

  ### Spec License
  s.license      = { :type => "Copyright", :text => 'Copyright 2015-2020 Cambridge Mobile Telematics, Inc' }

  ### Author Metadata
  s.author       = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }

  ### Platform Specifics
  s.platform     = :ios, "10.0"

  ### Source Location
  s.source       = { :git => "git@bitbucket.org:vodafoneautomotive/cmtfilterengineioslibrary.git", :tag => "#{s.version}" }

  ### Project Settings
  s.requires_arc = true
  
  s.vendored_frameworks = 'StaticFramework/CMTFilterEngineIOS.framework'
  s.library = 'c++'

end

