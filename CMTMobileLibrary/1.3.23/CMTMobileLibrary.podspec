Pod::Spec.new do |s|

  ### Summary information
  s.name             = "CMTMobileLibrary"
  s.version          = "1.3.23"
  s.summary          = "CMTMobileLibrary static binary library"
  s.description      = <<-DESC
                       The CMTMobileLibrary provides the following features
                       * Collect sensor data for telematics use
                       * Processed results on a drive by drive basis
                       * User-specific summary about driving performance
                       * Social features with telematics data
                       DESC
  s.homepage         = 'http://www.cmtelematics.com'

  ### Spec License                      
  s.license          = { :type => "Copyright", :text => 'Copyright 2015-present Cambridge Mobile Telematics, Inc' }
  s.author           = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }
  s.social_media_url = 'https://twitter.com/cmtelematics'


  ### Platform Specifics
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source              = { :git => "git@bitbucket.org:vodafoneautomotive/cmtmobilelibrarybinary.git", :tag => "#{s.version}" }
  s.source_files        = 'Library/Headers/*.h'
  s.vendored_libraries  = 'Library/libCMTMobileLibrary.a'
  s.public_header_files = 'Library/Headers/*.h'

  ### Project dependencies
  s.dependency 'Mantle'
  s.dependency 'AWSCore'
  s.dependency 'AWSS3'
  s.dependency 'Reachability'
  s.dependency 'CMTFilterEngineIOS', '~> 0.3.18'

  s.libraries = 'bz2', 'sqlite3', 'z'

end

