# used when testing the podspec (and pushing to the private specs)
Pod::Spec.new do |s|

  ### Summary information
  s.name             = "CMTMobileLibrary"
  s.version          = "5.0.1"
  s.summary          = "CMTMobileLibrary telematics framework"
  s.description      = <<-DESC
                       The CMTMobileLibrary provides the following features
                       * Collects sensor data for telematics use
                       * Uploads telematics data and retrieves trip data on a drive by drive basis
                       * Provides access to user-specific performance data
                       * User to user comparison data (leaderboards)
                       DESC
  s.homepage         = 'http://www.cmtelematics.com'

  ### Spec License                      
  s.license          = { :type => "Copyright", :text => 'Copyright 2012-present Cambridge Mobile Telematics, Inc' }
  s.author           = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }
  s.social_media_url = 'https://twitter.com/cmtelematics'


  ### Platform Specifics
  s.platform     = :ios, '9.0'
  s.requires_arc = true
  s.source = { :git => "git@bitbucket.org:vodafoneautomotive/cmtmobilelibrarybinary.git", :tag => "#{s.version}" }

  s.default_subspec = 'Framework'
  s.subspec 'Framework' do |framework|
    framework.vendored_frameworks = 'CMTMobileLibrary.framework'
  end
  
  s.subspec 'StaticLibrary' do |library|
    library.source_files        = 'Headers/*.h'
    library.vendored_libraries  = 'libCMTMobileLibrary.a'
    library.public_header_files = 'Headers/*.h'
  end
  
  ### Project dependencies
  s.dependency 'Mantle', '> 2.0.0'
  s.dependency 'AWSCore', '> 2.4.0'
  s.dependency 'AWSS3', '> 2.4.0'
  s.dependency 'CMTFilterEngineIOS', '0.8.0'
  s.dependency 'TrustKit', '>= 1.5.2'

  s.libraries = 'bz2', 'sqlite3', 'z'

end

