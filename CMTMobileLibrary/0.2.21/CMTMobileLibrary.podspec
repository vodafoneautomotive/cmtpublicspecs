Pod::Spec.new do |s|

  ### Summary information
  s.name             = "CMTMobileLibrary"
  s.version          = "0.2.21"
  s.summary          = "CMTMobileLibrary static binary library"
  s.description      = <<-DESC
                       The CMTMobileLibrary provides the following features
                       * Collect sensor data for telematics use
                       * Processed results on a drive by drive basis
                       * User-specific summary about driving performance
                       * Social features with telematics data
                       DESC
  s.homepage         = 'http://www.cmtelematics.com'

  ### Spec License                      
  s.license          = { :type => "Copyright", :text => 'Copyright 2015-present Cambridge Mobile Telematics, Inc' }
  s.author           = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }
  s.social_media_url = 'https://twitter.com/cmtelematics'


  ### Platform Specifics
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source              = { :git => "git@bitbucket.org:vodafoneautomotive/cmtmobilelibrarybinary.git", :tag => "#{s.version}" }
  s.source_files        = 'Library/Headers/*.h'
  s.vendored_libraries  = 'Library/libCMTMobileLibrary.a'
  s.public_header_files = 'Library/Headers/*.h'

  ### Project dependencies
  s.dependency 'Reachability', '~> 3.2'
  s.dependency 'Mantle', '~> 2.0.5'
  s.dependency 'FMDB', '~> 2.5'
  s.dependency 'AWSCore', '~> 2.3.0'
  s.dependency 'AWSS3', '~> 2.3.0'
  s.dependency 'AWSSQS', '~> 2.3.0'
  # users must pull CMTFilterEngine
  #  s.dependency 'CMTFilterEngineIOS', '~> 0.1.3'

  s.libraries = 'bz2', 'sqlite3', 'z'

end

