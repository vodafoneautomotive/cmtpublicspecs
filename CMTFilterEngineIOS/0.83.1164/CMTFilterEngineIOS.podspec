Pod::Spec.new do |s|

  ### Summary information
  s.name         = "CMTFilterEngineIOS"
  s.version      = "0.83.1164"
  s.summary      = "CMT real-time sensor processing library framework"
  s.description  = <<-DESC
                   Cambridge Mobile Telematics Real-Time Sensor Data Processing Library
                   DESC
  s.homepage     = "http://www.cmtelematics.com"

  ### Spec License
  s.license      = { :type => "Copyright", :text => 'Copyright 2012-2021 Cambridge Mobile Telematics, Inc' }

  ### Author Metadata
  s.author       = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }

  ### Platform Specifics
  s.platform     = :ios, "10.0"

  ### Source Location
  s.source       = { :git => "git@bitbucket.org:vodafoneautomotive/cmtfilterengine_ios_public.git", :tag => "#{s.version}" }

  ### Project Settings
  s.requires_arc = true
      
  s.vendored_frameworks = 'CMTFilterEngineIOS.framework'

  # needed because Xcode 12 introduces ability to build for arm64-based macs
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end

