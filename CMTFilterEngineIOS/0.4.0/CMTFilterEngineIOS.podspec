Pod::Spec.new do |s|

  ### Summary information
  s.name         = "CMTFilterEngineIOS"
  s.version      = "0.4.0"
  s.summary      = "CMT real-time sensor processing library"
  s.description  = <<-DESC
                   Cambridge Mobile Telematics Real-time sensor processing library 
                   DESC
  s.homepage       = "http://www.cmtelematics.com"

  ### Spec License
  s.license        = { :type => "Copyright", :text => 'Copyright 2015-present Cambridge Mobile Telematics, Inc' }

  ### Author Metadata
  s.author             = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }

  ### Platform Specifics
  s.platform     = :ios, "7.0"

  ### Source Location
  s.source       = { :git => "git@bitbucket.org:vodafoneautomotive/CMTFilterEngineIOSLibrary.git", :tag => "#{s.version}" }
  s.source_files = 'Library/*.{h,hpp}'
  s.vendored_libraries = "Library/libCMTFilterEngineIOS.a"
  s.public_header_files = 'Library/*.{h,hpp}'

  s.subspec 'libboost' do |boost|
    boost.preserve_paths = 'Library/libs/boost/include/**/*.{h,hpp,ipp}', 'Library/libs/boost/ios/*.a'
    boost.vendored_libraries = 'Library/libs/boost/ios/*.a'
    boost.libraries = 'boost'
    boost.xcconfig = { 'HEADER_SEARCH_PATHS' => "$(PODS_ROOT)/#{s.name}/Library/libs/include/**/*.{h,hpp,ipp}",
                       'OTHER_LDFLAGS' => '-lc++' }
  end
  
  # s.subspec 'boost' do |boost|
  #    boost.framework = 'boost'
  #    boost.preserve_paths = 'Library/boost.framework'
  #    boost.vendored_frameworks = 'Library/boost.framework'
  #    boost.xcconfig = { 'HEADER_SEARCH_PATHS' => "$(PODS_ROOT)/#{s.name}/Library/boost.framework/Headers/**/*.{h,hpp}",
  #                       'OTHER_LDFLAGS' => '-lc++' }
  # end 

  ### Project Settings
  s.requires_arc = true

end
