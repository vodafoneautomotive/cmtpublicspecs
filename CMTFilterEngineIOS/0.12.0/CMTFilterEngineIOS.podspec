Pod::Spec.new do |s|

  ### Summary information
  s.name         = "CMTFilterEngineIOS"
  s.version      = "0.12.0"
  s.summary      = "CMT real-time sensor processing library framework"
  s.description  = <<-DESC
                   Cambridge Mobile Telematics Real-time sensor processing library 
                   DESC
  s.homepage     = "http://www.cmtelematics.com"

  ### Spec License
  s.license      = { :type => "Copyright", :text => 'Copyright 2015-2017 Cambridge Mobile Telematics, Inc' }
p
  ### Author Metadata
  s.author       = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }

  ### Platform Specifics
  s.platform     = :ios, "8.0"

  ### Source Location
  s.source       = { :git => "git@bitbucket.org:vodafoneautomotive/cmtfilterengineioslibrary.git", :tag => "#{s.version}" }
  s.vendored_frameworks = 'CMTFilterEngineIOS.framework'

  ### Project Settings
  s.requires_arc = true

end
