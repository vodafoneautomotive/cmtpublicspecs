Pod::Spec.new do |s|

  ### Summary information
  s.name         = "CMTFilterEngineIOS"
  s.version      = "0.1.1"
  s.summary      = "CMT real-time sensor processing library"
  s.description  = <<-DESC
                   Cambridge Mobile Telematics Real-time sensor processing library 
                   DESC
  s.homepage       = "http://www.cmtelematics.com"

  ### Spec License
  s.license        = { :type => "Copyright", :text => 'Copyright 2015-present Cambridge Mobile Telematics, Inc' }

  ### Author Metadata
  s.author             = { "Cambridge Mobile Telematics, Inc" => "support@cmtelematics.com" }

  ### Platform Specifics
  s.platform     = :ios, "7.0"

  ### Source Location
  s.source       = { :git => "https://bitbucket.org/vodafoneautomotive/CMTFilterEngineIOSLibrary.git", :tag => "#{s.version}" }
  s.source_files = 'Library/Headers/*.{h,hpp}'
  s.vendored_libraries = 'Library/libCMTFilterEngineIOS.a'
  s.public_header_files = 'Library/Headers/*.{h,hpp}'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-l"CMTFilterEngineIOS"' }

  s.subspec 'boost' do |boost|
     boost.preserve_paths = 'Library/Frameworks'
     boost.vendored_frameworks = 'Library/Frameworks/boost.framework'
     boost.xcconfig = { 'HEADER_SEARCH_PATHS' => "$(PODS_ROOT}/#{s.name}/Library/Frameworks/Headers/**" }
  end 

  ### Project Settings
  s.requires_arc = true

end
